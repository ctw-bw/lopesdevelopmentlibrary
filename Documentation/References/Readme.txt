The documents in this directory don't apply directly to the LopesDevelopmentLibrary.
However, parts of these documents do apply to parts of the LDL:

Lopes User Manual.pdf (=manual for the therapist software) describes all gait parameters
(inside the GaitGeneration|GaitParameters block).

LopesTechnicalDocumentation.pdf (=technical document about the internals of the
therapist software) describes how (mathematically) the reference gait and reference
stiffness is calculated.
